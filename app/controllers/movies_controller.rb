class MoviesController < ApplicationController

  def show
    id = params[:id]
    @movie = Movie.find(id)
  end

  def index

    check_params(params)

    @movies = Movie.all
    if Movie.sort_params.include? @allowed_params['order_by']
      @movies = @movies.order(@allowed_params['order_by'])
    end


    allowed_params.each do |p|
      if !@allowed_params[p].nil? && Movie.column_names.include?(p)
        unless @allowed_params[p].empty?
          @movies = @movies.where("#{p} like ?", "%#{@allowed_params[p]}%")
        end
      end
    end

    @all_ratings = Movie.all_ratings
    @checked_ratings = if @allowed_params['ratings'].nil?
                         @all_ratings
                       else
                         @allowed_params['ratings'].keys
                       end
    if @checked_ratings
      @movies = @movies.where rating: @checked_ratings
    end

  end

  def check_params(parms)
    session[nil] = nil

    @allowed_params = get_allowed_params parms
    @allowed_params.merge!(get_allowed_params(parms['movie'])) unless params['movie'].nil?
    lacking_params = get_missing_params parms

    unless (session.keys & lacking_params).empty?
      fill_missing_params! @allowed_params, session, lacking_params
      flash.keep
      redirect_to movies_path(@allowed_params)
    end

    @allowed_params.entries.each do |key, value|
      session[key] = value if session_params.include? key
    end
  end

  def search

  end

  def new
    @all_ratings = Movie.all_ratings
  end

  def create
    redirect_path = nil
    begin
      @movie = Movie.create!(movie_params)
      title = @movie.title
      flash[:notice] = "#{title} was successfully created."
      redirect_path = movies_path
    rescue ActiveRecord::RecordInvalid
      flash[:notice] = "Failed to create #{title}"
      redirect_path = new_movie_path(params)
    end
    redirect_to redirect_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    redirect_path = nil
    begin
      @movie.update_attributes!(movie_params)
      title = @movie.title
      flash[:notice] = "#{title} was successfully updated."
      redirect_path = movie_path(@movie)
    rescue ActiveRecord::RecordInvalid
      flash[:notice] = "Failed to update #{title}"
      redirect_path = edit_movie_path(params)
    end

    redirect_to redirect_path
  end

  def destroy
    @movie = Movie.find(params[:id])
    title = @movie.title
    @movie.destroy
    flash[:notice] = "Movie '#{title}' deleted."
    redirect_to movies_path
  end

  private

  def movie_params
    params.require(:movie).permit(:title, :director, :description, :rating, :'release_date(1i)',
                                  :'release_date(2i)', :'release_date(3i)', :trailer)
  end

  def get_missing_params params
    missing_keys = []
    session_params.each do |param|
      unless params.include? param
        missing_keys << param
      end
    end
    missing_keys
  end

  def fill_missing_params!(params, session, missing_params)
    missing_params.each do |param|
      params[param] = session[param] unless session[param].nil?
    end
  end

  def get_allowed_params(parameters)
    result = Hash.new
    allowed_params.each do |param|
      result[param] = parameters[param] unless parameters[param].nil?
    end
    result
  end

  def allowed_params
    %w(director title) + session_params
  end

  def session_params
    %w(order_by ratings)
  end

end
