module MoviesHelper

  def oddness(count)
    count.odd? ?  "odd" :  "even"
  end

  def toggle_sort(name, value, *rest)
    tag = :th
    if params[:order_by] == value
      put_tag(tag, link_to(name,
                           movies_path(@allowed_params.merge(order_by: 'nothing'))),
              {:class => 'hilite'}, *rest)
    else
      put_tag(tag, link_to(name, movies_path(@allowed_params.merge(order_by: value))), *rest)
    end
  end

  def put_tag(tag, body, *rest)
    capture_haml do
      haml_tag tag, *rest do
        haml_concat body
      end
    end
  end
end
