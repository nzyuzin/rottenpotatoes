class Movie < ActiveRecord::Base

  def self.all_ratings
    %w(G PG PG-13 R)
  end

  validates :title, :presence => true
  validates :release_date, :presence => true
  validate :earlier_than_now
  validates :rating, :presence => true, :inclusion => { :in => Movie.all_ratings }
  validates :director, :presence => true

  def self.sort_params
    %w(title release_date director rating)
  end

  def earlier_than_now
    if release_date > Date.today
      errors.add(:release_date, "can't be in future")
    end
  end

end
