class AddDirector < ActiveRecord::Migration
  def change
    add_column :movies, :director, :string, default: '#Unspecified#'
  end
end
